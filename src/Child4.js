import React from 'react'
import { useRecoilState } from 'recoil'
import { businessNameState } from './atoms'

import './Child4.css';

export const Child4 = () => {
  const [businessName, setBusinessName] = useRecoilState(businessNameState)

  const onHandleClick = () => {
    setBusinessName('Xerox')
  }
    
  return (
    <div className='Child4'>
        Child4: { businessName } <button onClick={() => onHandleClick() }>Push</button>
    </div>
  )
}
