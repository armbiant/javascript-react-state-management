import { createContext } from 'react';

export let martyContext = null // The one and only context object shared between React Components.

export const getMartyContext = () => {
    if (martyContext === null) {
        martyContext = createContext(null)
        return martyContext
    } else {
        return martyContext
    }
}
