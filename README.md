# JavaScript React State Management
Simple React application that uses `useContext` and `Recoil` to share state between React components.

![](./docs/app.PNG)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Start the app: `npm start`

# UseContext
- https://beta.reactjs.org/apis/react/useContext
- `useContext` is a read only approach to pass state down to child components
- Since it is read only it cannot be used to update parent state from a child components
- `useContext` eliminates prop drilling

![](./docs/useContext.png)

# Recoil
- [Docs](https://recoiljs.org/docs/api-reference/core/useRecoilState)
- An Atom represents a piece of state
- Atoms can be read from and written to from any component
- All atoms are stored in a file called [atoms.js](./src/atoms.js)
- `useRecoilState` behaves similar to `useState`
- Changes made to a Recoil State Atom are propogated to all components
- `useRecoilValue` when a component will only READ from the state
- Recoil CAN behave just like `useContext` when `useRecoilValue` is used
- [Recoil](https://youtu.be/jsp4h0ZQsbw)

# Recoil Quick Reference
- `npm install recoil`
- Update `index.js`:
```
import { RecoilRoot } from 'recoil';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
     <RecoilRoot>
        <App />
     </RecoilRoot>
  </React.StrictMode>
);
```
- Create a file called [atoms.js](./src/atoms.js) and add your atom
- Consume the atom as if it were like `useState()`:
```
import { useRecoilState } from 'recoil'
import { businessNameState } from './atoms'

function App() {
 const [businessName, setBusinessName] = useRecoilState(businessNameState)
...
}
```
